//
//  DynamicHeightCollectionView.swift
//  DynamicCollection
//
//  Created by Walid on 8/2/2022.
//

import UIKit

protocol Layoutable{
    func refreshLayout()
}

class DynamicHeightCollectionView: UICollectionView, Layoutable {
    
    func refreshLayout() {
        self.collectionViewLayout.invalidateLayout()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if bounds.size != intrinsicContentSize {
            self.invalidateIntrinsicContentSize()
        }
    }
    override var intrinsicContentSize: CGSize {
        var newSize = collectionViewLayout.collectionViewContentSize
        newSize.width = UIScreen.main.bounds.width - contentInset.left - contentInset.right
        return newSize
//        return collectionViewLayout.collectionViewContentSize
    }
}

class DynamicHeightTableView: UITableView, Layoutable {
    
    func refreshLayout() {
        self.setNeedsLayout()
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if bounds.size != intrinsicContentSize {
            self.invalidateIntrinsicContentSize()
        }
    }
    override var intrinsicContentSize: CGSize {
        var newSize = contentSize
        newSize.width = UIScreen.main.bounds.width
        return newSize
//        return contentSize
    }
}

