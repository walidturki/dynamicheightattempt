//
//  EnvironmentViewController.swift
//  DynamicCollection
//
//  Created by Walid on 7/2/2022.
//

import UIKit

class EnvironmentViewController: UIViewController {

    @IBOutlet weak var collectionView: DynamicHeightCollectionView!
    
    var widgetObjects: [Environment] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
    }
    
    private func setupCollectionView() {
        widgetObjects = Environment.getData()
        let nibChips = UINib(nibName: "ChipsCollectionViewCell", bundle: .main)
        let nibTv = UINib(nibName: "ArrayCollectionViewCell", bundle: .main)
        collectionView.register(nibChips, forCellWithReuseIdentifier: "ChipsCollectionViewCell")
        collectionView.register(nibTv, forCellWithReuseIdentifier: "ArrayCollectionViewCell")
        
        collectionView.dataSource = self
    }

}

extension EnvironmentViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return widgetObjects.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell: UICollectionViewCell
        let env = widgetObjects[indexPath.row]
        if env.isVertical {
            ///tv
            let arrCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ArrayCollectionViewCell", for: indexPath) as! ArrayCollectionViewCell
            arrCell.setData(list: env.children)
            arrCell.title.text = env.title
            arrCell.containerDelegate = self.collectionView
            cell = arrCell
            
        }else {
            ///chips
            let chipsCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChipsCollectionViewCell", for: indexPath) as! ChipsCollectionViewCell
            chipsCell.setData(list: env.children)
            chipsCell.title.text = env.title
            chipsCell.containerDelegate = self.collectionView
            cell = chipsCell
        }
        return cell
    }
}
