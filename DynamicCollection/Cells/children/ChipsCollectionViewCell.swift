//
//  ChipsCollectionViewCell.swift
//  DynamicCollection
//
//  Created by Walid on 8/2/2022.
//

import UIKit

class ChipsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var collectionView: DynamicHeightCollectionView!
    var content: [String] = []
    var containerDelegate: Layoutable?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let nib = UINib(nibName: "ChipCollectionViewCell", bundle: .main)
        collectionView.register(nib, forCellWithReuseIdentifier: "ChipCollectionViewCell")
        collectionView.dataSource = self
    }
    
    func setData(list: [String]){
        content = list
        collectionView.reloadData()
    }
//    
//    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
//            let layoutAttributes = super.preferredLayoutAttributesFitting(layoutAttributes)
//            layoutIfNeeded()
//            layoutAttributes.frame.size = systemLayoutSizeFitting(UIView.layoutFittingCompressedSize, withHorizontalFittingPriority: .required, verticalFittingPriority: .fittingSizeLevel)
//            return layoutAttributes
//        }

}

extension ChipsCollectionViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return content.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChipCollectionViewCell", for: indexPath) as! ChipCollectionViewCell
        cell.label.text = content[indexPath.row]
        return cell
    }
}
