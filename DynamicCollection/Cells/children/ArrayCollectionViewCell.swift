//
//  arrayCollectionViewCell.swift
//  DynamicCollection
//
//  Created by Walid on 7/2/2022.
//

import UIKit

class ArrayCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var tableView: DynamicHeightTableView!
    
    var content: [String] = []
    var containerDelegate: Layoutable?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tvcellNib = UINib(nibName: "LabelTableViewCell", bundle: .main)
        tableView.register(tvcellNib, forCellReuseIdentifier: "LabelTableViewCell")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func setData(list: [String]){
        self.content = list
        self.tableView.reloadData()
//        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
//            self.content = list
//            self.tableView.reloadData()
//            self.containerDelegate?.refreshLayout()
//        })
        
    }

}

extension ArrayCollectionViewCell: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return content.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LabelTableViewCell") as! LabelTableViewCell
        cell.label?.text = content[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.content.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            self.containerDelegate?.refreshLayout()
        }
    }
    
}
